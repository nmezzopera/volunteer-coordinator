# volunteer-coordinator

[https://nmezzopera.gitlab.io/volunteer-coordinator](https://nmezzopera.gitlab.io/volunteer-coordinator)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```
