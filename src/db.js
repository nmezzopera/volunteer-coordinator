// Conveniently import this file anywhere to use db

import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const app = firebase.initializeApp({
  projectId: "volunteer-coordinator-2d7f8",
  apiKey: "AIzaSyCjOoeK5Wpww-Y6yIc4gyRzMZSLqS7nnQc",
  authDomain: "volunteer-coordinator-2d7f8.firebaseapp.com",
});

export const db = app.firestore();

export const auth = app.auth();
