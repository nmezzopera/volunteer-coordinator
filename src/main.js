import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import { firestorePlugin } from "vuefire";
import vuetify from "./plugins/vuetify";
import VueTelInputVuetify from "vue-tel-input-vuetify/lib";
import VueI18n from "vue-i18n";
import messages from "./messages";

Vue.use(VueI18n);

Vue.use(firestorePlugin);

Vue.use(VueTelInputVuetify, {
  vuetify,
});

Vue.config.productionTip = false;

const i18n = new VueI18n({
  locale: "en", // set locale
  messages, // set locale messages
});

new Vue({
  router,
  vuetify,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
