import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import { auth } from "../db";

Vue.use(VueRouter);

const pathWithoutAuth = ["/login", "/volunteer"];

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/login",
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Login.vue"),
  },
  {
    path: "/refugee",
    name: "Refugee",
    component: () =>
      import(/* webpackChunkName: "refugee" */ "../views/Refugee.vue"),
  },
  {
    path: "/refugee/:needs",
    name: "VolunteerList",
    component: () =>
      import(/* webpackChunkName: "refugee" */ "../views/VolunteerList.vue"),
  },
  {
    path: "/volunteer",
    name: "Volunteer",
    component: () =>
      import(/* webpackChunkName: "volunteer" */ "../views/Volunteer.vue"),
  },
];

const router = new VueRouter({
  routes,
  mode: "history",
  base: process.env.VUE_APP_BASE_PATH,
});

router.beforeEach(async (to, from, next) => {
  const user = await new Promise((resolve) => {
    auth.onAuthStateChanged((u) => resolve(u));
  });

  if (!user && !pathWithoutAuth.includes(to.path)) {
    next("/login");
  }
  next();
});

export default router;
