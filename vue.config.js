process.env.VUE_APP_BASE_PATH =
  process.env.NODE_ENV === "production"
    ? "/" + process.env.CI_PROJECT_NAME + "/"
    : "/";

module.exports = {
  publicPath: process.env.VUE_APP_BASE_PATH,
  transpileDependencies: ["vuetify", "vue-tel-input-vuetify"],
};
